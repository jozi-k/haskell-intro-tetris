module Main where

import Graphics.Gloss
import Graphics.Gloss.Data.Picture
import Graphics.Gloss.Interface.Pure.Game
import Data.List
import System.Random

{- TODO: down arrow handler, score/levels (increasing speed of piece), show next piece, etc. -}
main :: IO ()
main = 
  play
  window
  background 
  1 
  initialState 
  render 
  handleKeys 
  update

window :: Display
window = InWindow "Tetris" (500, 640) (10, 10)

background :: Color
background = black

blockSize :: Float
blockSize = 32

data Block = 
  Void | 
  Fill Color
    deriving (Eq, Show)

type Grid = [[Block]]
{-
  board, piece, x and y coordinate of piece, random generator
-}
type GameState = (Grid, Grid, Float, Float, StdGen)

(gen, initialPiece) = randomPieceGen (snd seedGen)

initialState :: GameState
initialState = (emptyBoard, initialPiece, 4, 0, gen)

emptyBoard :: Grid
emptyBoard = replicate 20 (replicate 10 Void)

render :: GameState -> Picture
render (board, piece, x, y, _) = pictures((renderBoard board 0 0) ++ (renderBoard piece (x * blockSize) (y * blockSize)))

renderBoard :: Grid -> Float -> Float -> [Picture]
renderBoard [] _ _ = []
renderBoard (row:restOfRows) x y = (renderRow row x y) ++ (renderBoard restOfRows x (y + blockSize))

renderRow :: [Block] -> Float -> Float -> [Picture]
renderRow [] _ _                      = []
renderRow  (Void : blocks)    x y = renderRow blocks (x + blockSize) y
renderRow ((Fill c) : blocks) x y = (color c (translate (x-234) (304-y) (rectangleSolid blockSize blockSize))) : (renderRow blocks (x+blockSize) y)

handleKeys :: Event -> GameState -> GameState
handleKeys (EventKey (SpecialKey KeyLeft ) Down _ _) (board, piece, x, y, gen)
    | inBounds piece (x - 1) y == False = (board, piece, x, y, gen)
    | inBounds piece (x - 1) y == True  = (board, piece, x - 1, y, gen) 
handleKeys (EventKey (SpecialKey KeyRight ) Down _ _) (board, piece, x, y, gen)
    | inBounds piece (x + 1) y == False = (board, piece, x, y, gen)
    | inBounds piece (x + 1) y == True  = (board, piece, x + 1, y, gen)
handleKeys (EventKey (SpecialKey KeyUp   ) Down _ _) (board, piece, x, y, gen) = (board, myRotate piece, x, y, gen)
handleKeys _ state = state

{- Cannot be named rotate as it collides with some other function -}
myRotate :: Grid -> Grid
myRotate piece = transpose $ map reverse piece

update :: Float -> GameState -> GameState
update inc (board, piece, x, y, gen)
    | canFall == True = (board, piece, x, y + 1, gen) 
    | canFall == False = (clearedRows, randomPiece, 4, 0, newGen)
  where
    canFall = validPlace board piece x y
    piecePlusBoard = mergeGrids board piece x y
    clearedRows = clearFullRows piecePlusBoard
    (newGen, randomPiece) = randomPieceGen gen

validPlace :: Grid -> Grid -> Float -> Float -> Bool
validPlace board piece x y = inBounds piece x y && not (overlap board piece x y)

{- TODO add move left/right check -}
inBounds :: Grid -> Float -> Float -> Bool
inBounds piece x y = (maxPositionY <= 10) && (x >= 0) && (maxPositionX <= 10)
  where 
    zipWithIndex = zip [1..] piece
    containBlocks = filter (\(a, b) -> elem (Fill red) b) zipWithIndex
    maxY = maximumBy (\(x, _) (y, _) -> compare x y) containBlocks
    maxPositionY = y + (fst maxY)
    blocksCount = map (\row -> length $ filter (\x -> x == Fill red) row) piece
    maxPositionX = round x + maximum blocksCount

mergeGrids :: Grid -> Grid -> Float -> Float -> Grid
mergeGrids [] _ _ _ = []
mergeGrids board [] _ _ = board
mergeGrids (boardRow : boardRows) piece@(pieceRow: pieceRows) x y
    | y > 0  = boardRow : mergeGrids boardRows piece x (y - 1)
    | y == 0 = mergeRows boardRow pieceRow x : mergeGrids boardRows pieceRows x y

mergeRows :: [Block] -> [Block] -> Float -> [Block]
mergeRows []    _  _ = []
mergeRows board [] _ = board
mergeRows (boardBlock : boardBlocks) piece@(pieceBlock : pieceBlocks) x
    | x == 0 = (if pieceBlock == Void then boardBlock else pieceBlock) : mergeRows boardBlocks pieceBlocks x
    | x > 0  = boardBlock : mergeRows boardBlocks piece (x - 1)

{- Quite adhoc algorithm, could be definitely improved -}
overlap :: Grid -> Grid -> Float -> Float -> Bool
overlap board piece x y = elem True isCollision
  where 
    rowWithIndex = zip [1..] piece
    isCollision = map (\(idx, row) -> collide board row x y idx) rowWithIndex
    
collide :: Grid -> [Block] -> Float -> Float -> Int -> Bool
collide board rowPiece x y idx = elem True res
  where
    rowToFall = board !! (round y + idx)
    dropped = drop (round x) rowToFall
    res = zipWith isColliding rowPiece dropped 

isColliding :: Block -> Block -> Bool
isColliding Void Void = False
isColliding Void _    = False
isColliding _    Void = False
isColliding _    _    = True

clearFullRows :: Grid -> Grid
clearFullRows grid = emptyRows ++ activeRows
  where
    (activeRows, clearedRows) = partition (\x -> elem Void x) grid
    emptyRows = replicate (length clearedRows) (replicate 10 Void)

{- Random generator stuff -}
seedGen :: (Integer, StdGen)
seedGen = random (mkStdGen 100)

randomPieceGen :: StdGen -> (StdGen, Grid)
randomPieceGen oldGen = (newGen, newPiece)
  where 
    (index, newGen) = randomR (0, (length pieces) - 1) oldGen
    newPiece = pieces !! index

{- Pieces definition -}
t :: Grid
t = [
    [Void    , Fill red, Void],
    [Fill red, Fill red, Fill red]
    ]

i :: Grid
i = [
    [Fill red],
    [Fill red],
    [Fill red]
    ]

l :: Grid
l = [
  [Fill red, Void],
  [Fill red, Void],
  [Fill red, Fill red]
  ]

o :: Grid
o = [
  [Fill red, Fill red],
  [Fill red, Fill red]
  ]

s :: Grid
s = [
  [Void    , Fill red, Fill red],
  [Fill red, Fill red, Void    ]
  ]

pieces = [t, i, l, o, s]